package IBusiness.delegate;

import IBusiness.delegate.soap.mail.InfoMailDTO;

public interface IMailBusinessDelegate {

    void envoyerMailConfirmation(InfoMailDTO info);
}
