
package IBusiness.delegate.soap.mail;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour envoyerMail complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="envoyerMail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoMailDTO" type="{http://wsmail.afcepf.fr}infoMailDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "envoyerMail", propOrder = {
    "infoMailDTO"
})
public class EnvoyerMail {

    protected InfoMailDTO infoMailDTO;

    /**
     * Obtient la valeur de la propri�t� infoMailDTO.
     * 
     * @return
     *     possible object is
     *     {@link InfoMailDTO }
     *     
     */
    public InfoMailDTO getInfoMailDTO() {
        return infoMailDTO;
    }

    /**
     * D�finit la valeur de la propri�t� infoMailDTO.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoMailDTO }
     *     
     */
    public void setInfoMailDTO(InfoMailDTO value) {
        this.infoMailDTO = value;
    }

}
