
package IBusiness.delegate.soap.mail;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour envoyerMailResponse complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="envoyerMailResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultMail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "envoyerMailResponse", propOrder = {
    "resultMail"
})
public class EnvoyerMailResponse {

    protected boolean resultMail;

    /**
     * Obtient la valeur de la propri�t� resultMail.
     * 
     */
    public boolean isResultMail() {
        return resultMail;
    }

    /**
     * D�finit la valeur de la propri�t� resultMail.
     * 
     */
    public void setResultMail(boolean value) {
        this.resultMail = value;
    }

}
