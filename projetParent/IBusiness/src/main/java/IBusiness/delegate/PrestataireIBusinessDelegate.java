package IBusiness.delegate;

import java.util.List;

import Dto.Periode;
import Dto.Prestataire;

public interface PrestataireIBusinessDelegate 
{
	List<Prestataire> tousPrestataires();
	List<Prestataire> tousPrestatairesParTypePrestation(int idPrestation);
	
	List<Prestataire> prestatairesParTypePrestationEtPeriode(int idPrestation, Periode periode);
	Prestataire reserverPrestatairesPourPeriode(int idPrestataire, Periode periode);
	void annulerPrestatairesPourPeriode(int idPrestataire, Periode periode);
}
