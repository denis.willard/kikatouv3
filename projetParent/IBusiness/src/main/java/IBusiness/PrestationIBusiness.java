package IBusiness;

import Entities.Prestation;

import java.util.List;

public interface PrestationIBusiness {

	Prestation create(Prestation p);
	Prestation delete(Prestation p);
	Prestation update(Prestation p);
	Prestation find(int id);
	List<Prestation> findByIdEvent(int id);
	void deletePrestation(Prestation p);
}
