package IBusiness;

import java.util.List;

import Entities.TypeEvent;

public interface TypeEventIBusiness {

	TypeEvent create(TypeEvent typeEvent);
	TypeEvent delete(TypeEvent typeEvent);
	TypeEvent update(TypeEvent typeEvent);
	TypeEvent find(int id);
	TypeEvent findByNom(String nom);
	List<TypeEvent> getAll();

}
