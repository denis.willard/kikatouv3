package IDao;

import Entities.Prestation;

import java.util.List;

public interface PrestationIDao extends IDAO{

	List<Prestation> findByIdEvent(int id);
	void deletePrestation(Prestation p);
}
