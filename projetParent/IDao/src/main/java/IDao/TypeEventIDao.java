package IDao;

import java.util.List;

import Entities.TypeEvent;

public interface TypeEventIDao extends IDAO {
	List<TypeEvent> getAll();
	TypeEvent getByNom(String nom);

}
