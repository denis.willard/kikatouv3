package Business;

import java.util.Arrays;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import Entities.Ville;
import IBusiness.VilleIBusiness;
import IDao.VilleIDao;

@Remote(VilleIBusiness.class)
@Stateless
public class VilleBusiness implements VilleIBusiness {

	@EJB
	private VilleIDao proxyVille;

	@Override
	public Ville create(Ville ville) {
		Ville v = proxyVille.findByCp(ville.getCodePostal());
		if (v != null) {			
			return v;
		}
		else {
			getCoordonneesGeographiques(ville); // appel à https://geo.api.gouv.fr pour récupérer les coordonnées géographiques
			proxyVille.create(ville);
			int id = proxyVille.getLastId();
			ville.setId(id);
			return ville;
		}
	}

	@Override
	public Ville delete(Ville ville) {
		proxyVille.delete(ville);
		return ville;
	}

	@Override
	public Ville update(Ville ville) {
		// TODO Auto-generated method stub
		proxyVille.update(ville);
		return ville;
	}

	@Override
	public Ville find(int id) {
		return proxyVille.find(Ville.class, id);
	}
	
	private void getCoordonneesGeographiques(Ville ville) {
		// lectures des coordonnées géographiques à partir du code postal
		Client jaxrs2client;
		String url ="https://geo.api.gouv.fr/"; 
		jaxrs2client = ClientBuilder.newClient();
		WebTarget convTarget = jaxrs2client.target(url)
				.path("communes")
				.queryParam("codePostal", ville.getCodePostal())
				.queryParam("fields", "nom,centre")
				.queryParam("format", "json")
				;

		JsonArray response = convTarget.request(MediaType.APPLICATION_JSON_TYPE).get(JsonArray.class);
		
		for (JsonObject jsonObject : response.getValuesAs(JsonObject.class)) {
		    JsonObject centre = jsonObject.getJsonObject("centre");
			JsonArray coordinates = centre.getJsonArray("coordinates");
			// transfert des coordonnées 
			ville.setLongitude(coordinates.getJsonNumber(0).doubleValue());
			ville.setLatitude(coordinates.getJsonNumber(1).doubleValue());
			if (response.size() == 1) {
				// une seule commune avec ce code postal
				ville.setNom(jsonObject.getString("nom"));
			}
			break;
		}		
		
	}
}
