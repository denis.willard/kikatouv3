package Business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import Entities.TypeEvent;
import IBusiness.TypeEventIBusiness;
import IDao.TypeEventIDao;

@Remote(TypeEventIBusiness.class)
@Stateless
public class TypeEventBusiness  implements TypeEventIBusiness{

	@EJB
	private TypeEventIDao proxyTypeEvent;
	
	@Override
	public TypeEvent create(TypeEvent typeEvent) {
		proxyTypeEvent.create(typeEvent);
		return typeEvent;
	}

	@Override
	public TypeEvent delete(TypeEvent typeEvent) {
		proxyTypeEvent.delete(typeEvent);
		return typeEvent;
	}

	@Override
	public TypeEvent update(TypeEvent typeMouvement) {
		proxyTypeEvent.update(typeMouvement);
		return typeMouvement;
	}

	@Override
	public TypeEvent find(int id) {
		return proxyTypeEvent.find(TypeEvent.class, id);
	}

	@Override
	public TypeEvent findByNom(String nom) {
		return proxyTypeEvent.getByNom(nom);
	}

	@Override
	public List<TypeEvent> getAll() {
		return proxyTypeEvent.getAll();
	}

}
