package Business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import Entities.Event;
import IBusiness.EventIBusiness;
import IDao.EventIDao;

@Remote(EventIBusiness.class)
@Stateless
public class EventBusiness implements EventIBusiness{

	@EJB
	private EventIDao proxyEvent;
	
	@Override
	public Event create(Event e) {
		proxyEvent.create(e);
		int id = proxyEvent.getLastId();
		e.setId(id);
		return e;
	}

	@Override
	public Event delete(Event e) {
		proxyEvent.delete(e);
		return e;
	}

	@Override
	public Event update(Event e) {
		proxyEvent.update(e);
		return e;
	}

	@Override
	public Event find(int id) {
		return proxyEvent.find(Event.class, id);
	}

	@Override
	public List<Event> findByIdCommande(int id) {
		return proxyEvent.findByIdCommande(id);
	}

	@Override
	public List<Event> getAllByIdUser(int id) {
		return proxyEvent.getAllByIdUser(id);
	}

	@Override
	public List<Event> findNoRetour() {
		return proxyEvent.findNoRetour();
	}
	
	@Override
	// récupération de l'événement correspondant au panier en cours de l'utilisateur.
	// On le reconnait par une date de validation non renseignée
	public Event getCurrentEventByIdUser(int id) {
		Event result = null;
		List<Event> events = proxyEvent.getAllByIdUser(id);
		for (Event event : events) {
			result = event;
			if (event.getDateValidation() == null) {
				result = event;
			}
		}
		return result;		
	}
	
}
