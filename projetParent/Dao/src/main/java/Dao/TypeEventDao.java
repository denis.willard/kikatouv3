package Dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Entities.TypeEvent;
import IDao.TypeEventIDao;

@Remote(TypeEventIDao.class)
@Stateless
public class TypeEventDao extends DAO implements TypeEventIDao {
	
	@PersistenceContext(unitName="DidiDS")
	private EntityManager em;

	@Override
	public List<TypeEvent> getAll() {
		Query query = em.createQuery("SELECT t from TypeEvent t");
		List<TypeEvent> typesEvent = query.getResultList();
		return typesEvent;
	}
	@Override
	public TypeEvent getByNom(String nom) {
		Query query = em.createQuery("SELECT t from TypeEvent t WHERE t.nom=:nom",TypeEvent.class);
		query.setParameter("nom", nom);
		TypeEvent result = (TypeEvent) query.getSingleResult();
		return result;
	}


}
