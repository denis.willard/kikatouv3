package ManagedBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Dto.TypePrestation;
import Entities.Article;
import IBusiness.ArticleIBusiness;
import IBusiness.delegate.TypePrestationIBusinessDelegate;

@ManagedBean(name = "mbTypePrestation")
@SessionScoped
public class TypePrestationMB implements Serializable {
    private static final long serialVersionUID = 1L;

    @EJB
    private TypePrestationIBusinessDelegate proxyTypePrestation;

    private List<TypePrestation> AllTypePrestation = new ArrayList<TypePrestation>();

    private boolean serviceIndisponible = false;

    //@PostConstruct
    public void init() {
        try {
            serviceIndisponible = false;
            AllTypePrestation = proxyTypePrestation.tousTypesPrestations();
        } catch (Exception e) {
            System.out.println("Prestataires non disponibles");
            serviceIndisponible = true;
        }
    }

    // Getters Setters --------------------------------------

    public List<TypePrestation> getAllTypePrestation() {
        return AllTypePrestation;
    }

    public void setAllTypePrestation(List<TypePrestation> allTypePrestation) {
        AllTypePrestation = allTypePrestation;
    }

    public boolean isServiceIndisponible() {
        return serviceIndisponible;
    }

    public void setServiceIndisponible(boolean serviceIndisponible) {
        this.serviceIndisponible = serviceIndisponible;
    }
}
