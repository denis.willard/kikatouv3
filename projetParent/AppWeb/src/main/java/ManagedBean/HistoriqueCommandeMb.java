package ManagedBean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Request;

import Entities.Commande;
import Entities.Event;
import Entities.Prestation;
import Entities.User;
import IBusiness.CommandeIBusiness;
import IBusiness.EventIBusiness;
import IBusiness.PrestationIBusiness;
import net.bootsfaces.C;

@ManagedBean(name = "mbHisto")
@SessionScoped
public class HistoriqueCommandeMb implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManagedProperty(value = "#{mblogin.user}")
    private User user;

    @ManagedProperty(value = "#{mbcommande}")
    private CommandeMb commandeMb;


    @EJB
    private EventIBusiness proxyEvent;
    @EJB
    private PrestationIBusiness proxyPrestation;

    @EJB
    private CommandeIBusiness proxyCommande;
    private Event event1 = new Event();
    private List<calculEvent> histoEvent = new ArrayList<calculEvent>();
    private List<Event> events = new ArrayList<Event>();
    private List<Commande> commandes = new ArrayList<Commande>();
    private List<Prestation> prestations = new ArrayList<>();
    private Double totalCommande = 0d;
    private double prixTotal = 0d;
    private boolean redirectFromBanque = false;
 

    @PostConstruct
    public void init() throws ServletException, IOException {
    	
    	ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
    	Map params = ec.getRequestParameterMap();
    	String response = (String) params.get("banque");
    	
    	if(response.equals("true")) {
    		this.redirectFromBanque = true;
    	} else {
    		this.redirectFromBanque = false;
    	}
    	
        if (redirectFromBanque) {
            commandeMb.ajoutDateValidation();
            redirectFromBanque = false;
        }
        histoEvent.clear();
        events = proxyEvent.getAllByIdUser(user.getId());
        for (Event e : events) {
            calculEvent cE = new calculEvent();
            cE.setE(e);
            long diffInMillies = Math.abs(e.getDateDebut().getTime() - e.getDateFin().getTime());
            Long diDi = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) + 1;
            commandes = proxyCommande.findByIdEvent(e.getId());
            prestations = proxyPrestation.findByIdEvent(e.getId());
            for (Commande commande : commandes) {
                totalCommande += commande.getArticle().getPrix() * commande.getQuantite() * (diDi);
            }
            for (Prestation prestation : prestations) {
                totalCommande += (prestation.getPrixHoraire() * prestation.getDuree());
            }
            cE.setPrix(totalCommande);
            histoEvent.add(cE);
            totalCommande = 0d;
        }
    }
    
    

    public List<Commande> commandeByIdEvent(int id) {
        commandes = proxyCommande.findByIdEvent(id);
        prestations = proxyPrestation.findByIdEvent(id);
        prixTotal = 0d;
        Event toto = proxyEvent.find(id);
        long diffInMillies = Math.abs(toto.getDateDebut().getTime() - toto.getDateFin().getTime());
        Long diDi = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) + 1;
        for (Commande commande : commandes) {
            prixTotal += (commande.getArticle().getPrix() * commande.getQuantite() * diDi);
        }
        for (Prestation prestation : prestations) {
            prixTotal += (prestation.getPrixHoraire() * prestation.getDuree());
        }
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, "/commande/HistoriqueCommande.xhtml?faces-redirect=true");
        return commandes;
    }

    //pojo


    public EventIBusiness getProxyEvent() {
        return proxyEvent;
    }


    public Event getEvent1() {
        return event1;
    }

    public void setEvent1(Event event1) {
        this.event1 = event1;
    }

    public void setProxyEvent(EventIBusiness proxyEvent) {
        this.proxyEvent = proxyEvent;
    }

    public CommandeIBusiness getProxyCommande() {
        return proxyCommande;
    }

    public void setProxyCommande(CommandeIBusiness proxyCommande) {
        this.proxyCommande = proxyCommande;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public List<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Double getTotalCommande() {
        return totalCommande;
    }

    public void setTotalCommande(Double totalCommande) {
        this.totalCommande = totalCommande;
    }

    public List<calculEvent> getHistoEvent() {
        return histoEvent;
    }

    public void setHistoEvent(List<calculEvent> histoEvent) {
        this.histoEvent = histoEvent;
    }

    public double getPrixTotal() {
        return prixTotal;
    }


    public void setPrixTotal(double prixTotal) {
        this.prixTotal = prixTotal;
    }

    public List<Prestation> getPrestations() {
        return prestations;
    }

    public void setPrestations(List<Prestation> prestations) {
        this.prestations = prestations;
    }

    public boolean isRedirectFromBanque() {
        return redirectFromBanque;
    }

    public void setRedirectFromBanque(boolean redirectFromBanque) {
        this.redirectFromBanque = redirectFromBanque;
    }

    public class calculEvent {

        private Event e;
        private Double prix;

        public Event getE() {
            return e;
        }

        public void setE(Event e) {
            this.e = e;
        }

        public Double getPrix() {
            return prix;
        }

        public void setPrix(Double prix) {
            this.prix = prix;
        }
    }

    public CommandeMb getCommandeMb() {
        return commandeMb;
    }

    public void setCommandeMb(CommandeMb commandeMb) {
        this.commandeMb = commandeMb;
    }


    
}
