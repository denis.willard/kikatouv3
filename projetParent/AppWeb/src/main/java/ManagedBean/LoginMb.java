package ManagedBean;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import Entities.Event;
import Entities.User;
import IBusiness.EventIBusiness;
import IBusiness.UserIBusiness;

@ManagedBean(name = "mblogin")
@SessionScoped
public class LoginMb implements Serializable {

    private static final long serialVersionUID = 1L;


    @EJB
    private UserIBusiness proxyUser;

    @EJB
    private EventIBusiness proxyEvent;

    private User user;
    private String log;
    private String mdp;
    private String msg = "";

    // événement correspondant au panier courant de l'utilisateur
    private Event event;

    // si isConnected = 0, on affiche le bouton "Connexion" et on masque le bouton "Déconnexion"
    // et si isConnected = 1, on affiche le bouton "Déconnexion" et on masque le bouton "Connexion"
    private int isConnected = 0;


    public String connexion() {
        user = proxyUser.connection(log, mdp);
        if (user != null) {
            this.setEvent(proxyEvent.getCurrentEventByIdUser(user.getId()));// récupération panier en cours
            isConnected = 1;
            switch (user.getClientDTO().getRole()) {
                case "Client":
                case "Prestataire":
                    return "/categorie/affichageSousCat.xhtml?id=1&faces-redirect=true";
                case "Admin":
                    return "/stock/affichageArticleStockDispo.xhtml?faces-redirect=true";
            }
        }
        return msg = "* L'utilisateur ou le mot de passe est incorrect";
    }

    public String deconnection() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, "/connexion.xhtml?faces-redirect=true");
        FacesContext.getCurrentInstance()
                .getExternalContext()
                .invalidateSession();
        isConnected = 0;
        return "/AppWeb/faces/accueil.xhtml?faces-redirect=true";
    }

    public String pageconnexion() {
        return "/connexion.xhtml";
    }


    public UserIBusiness getProxyUser() {
        return proxyUser;
    }


    public void setProxyUser(UserIBusiness proxyUser) {
        this.proxyUser = proxyUser;
    }


    public User getUser() {
        return user;
    }


    public void setUser(User user) {
        this.user = user;
    }


    public String getLog() {
        return log;
    }


    public void setLog(String log) {
        this.log = log;
    }


    public String getMdp() {
        return mdp;
    }


    public void setMdp(String mdp) {
        this.mdp = mdp;
    }


    public String getMsg() {
        return msg;
    }


    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getIsConnected() {
        return isConnected;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public void setIsConnected(int isConnected) {
        this.isConnected = isConnected;
    }

}
