package ManagedBean;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import Dto.Periode;
import Dto.TypePrestation;
import Entities.Commande;
import Entities.Event;
import Entities.Prestation;
import IBusiness.CommandeIBusiness;
import IBusiness.EventIBusiness;
import IBusiness.PrestationIBusiness;
import IBusiness.delegate.IMailBusinessDelegate;
import IBusiness.delegate.PrestataireIBusinessDelegate;
import IBusiness.delegate.soap.mail.InfoMailDTO;
import dtoClients.TransactionDTO;

@ManagedBean(name = "mbcommande")
@SessionScoped
public class CommandeMb implements Serializable {

    private static final long serialVersionUID = 1L;


    @EJB
    private CommandeIBusiness proxyCommande;

    @EJB
    private PrestationIBusiness proxyPrestation;

    @EJB
    private PrestataireIBusinessDelegate proxyPrestataire;

    @EJB
    private EventIBusiness proxyEvent;

    @EJB
    private IMailBusinessDelegate mailBusinessDelegate;

    private Double prixTotal = 0d;
    private int quantiteTotalArticle = 0;
    private Event event = new Event();
    private List<Commande> commandes = new ArrayList<Commande>();
    private List<Prestation> prestations = new ArrayList<>();
    private String msg;
    private Long dureeReserv;
    private boolean paiementValide;
    private String hash;
    
	private Client jaxrs2client;
	private String url ="http://localhost:8888/WSPaiement/rest/validation/"; 

    @ManagedProperty(value = "#{mblogin}")
    private LoginMb loginmb;

    //@PostConstruct
    public void init() {
        try {
            //calcul de la date
            long diffInMillies = Math.abs(loginmb.getEvent().getDateDebut().getTime() - loginmb.getEvent().getDateFin().getTime());
            dureeReserv = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) + 1;

            commandes = proxyCommande.findByIdEvent(loginmb.getEvent().getId());
            prestations = proxyPrestation.findByIdEvent(loginmb.getEvent().getId());
            prixTotal = 0d;
            quantiteTotalArticle = 0;
            for (Commande commande : commandes) {
                prixTotal += (commande.getQuantite() * commande.getArticle().getPrix() * (dureeReserv));
                quantiteTotalArticle += commande.getQuantite();
            }
            for (Prestation prestation : prestations) {
                prixTotal += (prestation.getPrixHoraire() * prestation.getDuree());
                quantiteTotalArticle += 1;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteCommande(Commande c) {
        proxyCommande.deleteCommande(c);
    }

    public void deletePrestation(Prestation p) {
        DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
        Periode periode = new Periode();
        periode.setDateDebut(shortDateFormat.format(p.getDateDebut()));
        System.out.println("Date début " + periode.getDateDebut());
        System.out.println("Id " + p.getIdPrestataire());
        periode.setDateFin(shortDateFormat.format(p.getDateFin()));
        System.out.println("Date début " + periode.getDateFin());
        proxyPrestataire.annulerPrestatairesPourPeriode(p.getIdPrestataire(), periode);
        proxyPrestation.deletePrestation(p);
    }

    public void paiement() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        try {
            Client client = ClientBuilder.newClient();
            TransactionDTO transactionDTO = new TransactionDTO();
            transactionDTO.setMontant(prixTotal);
            transactionDTO.setUrl("http://localhost:7979/AppWeb/faces/event/HistoriqueEvent.xhtml");
            Response response = client.target("http://localhost:8888/WSPaiement/rest/paiement")
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .post(Entity.entity(transactionDTO, MediaType.APPLICATION_JSON_TYPE));
            hash = response.readEntity(String.class);
            externalContext.redirect("http://localhost:5001/form/"+hash);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Redirection impossible vers Event");
        }
    }

    public Double CalculPrixCommande() {
        return prixTotal;
    }

    public void ajoutDateValidation() {
        
		
    	String paymentValidate = "true";
    	
        
        if (paymentValidate.equals("true")) {
   
           loginmb.setEvent(proxyEvent.getCurrentEventByIdUser(loginmb.getUser().getId()));
            prestations = proxyPrestation.findByIdEvent(loginmb.getEvent().getId());
            if (prestations.size() > 0) {
                for (Prestation prestation : prestations) {
                    InfoMailDTO infoMailDTO = new InfoMailDTO();
                    infoMailDTO.setAdressePrestataire(prestation.getPrestataire().getEmail());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    infoMailDTO.setDateDebutReservation(simpleDateFormat.format(prestation.getDateDebut()));
                    infoMailDTO.setNomPrestataire(prestation.getPrestataire().getNom());
                    infoMailDTO.setNomClient(loginmb.getUser().getNom());
                    infoMailDTO.setPrenomClient(loginmb.getUser().getPrenom());
                    infoMailDTO.setPrenomPrestataire(prestation.getPrestataire().getPrenom());
                    infoMailDTO.setDuree(Integer.toString(prestation.getDuree()));
                    mailBusinessDelegate.envoyerMailConfirmation(infoMailDTO);
                }
            }
            event = loginmb.getEvent();
            event.setDateValidation(new Date());
            proxyEvent.update(event);
            msg = "Votre commande est validée. Merci d'avoir commandé chez nous! :)";
            loginmb.setEvent(null);
            event = new Event();
            commandes = new ArrayList<>();
            prestations = new ArrayList<>();
            quantiteTotalArticle = 0;
            prixTotal = 0d;
        }
    }


    public CommandeIBusiness getProxyCommande() {
        return proxyCommande;
    }

    public void setProxyCommande(CommandeIBusiness proxyCommande) {
        this.proxyCommande = proxyCommande;
    }


    public List<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }


    public Double getPrixTotal() {
        return prixTotal;
    }

    public void setPrixTotal(Double prixTotal) {
        this.prixTotal = prixTotal;
    }

    public int getQuantiteTotalArticle() {
        return quantiteTotalArticle;
    }

    public void setQuantiteTotalArticle(int quantiteTotalArticle) {
        this.quantiteTotalArticle = quantiteTotalArticle;
    }

    public LoginMb getLoginmb() {
        return loginmb;
    }

    public void setLoginmb(LoginMb loginmb) {
        this.loginmb = loginmb;
    }


    public Event getEvent() {
        return event;
    }


    public void setEvent(Event event) {
        this.event = event;
    }


    public EventIBusiness getProxyEvent() {
        return proxyEvent;
    }


    public void setProxyEvent(EventIBusiness proxyEvent) {
        this.proxyEvent = proxyEvent;
    }


    public String getMsg() {
        return msg;
    }


    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getDureeReserv() {
        return dureeReserv;
    }

    public List<Prestation> getPrestations() {
        return prestations;
    }

    public void setPrestations(List<Prestation> prestations) {
        this.prestations = prestations;
    }

    public void setDureeReserv(Long dureeReserv) {
        this.dureeReserv = dureeReserv;
    }

    public boolean isPaiementValide() {
        return paiementValide;
    }

    public void setPaiementValide(boolean paiementValide) {
        this.paiementValide = paiementValide;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
