package ManagedBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import Entities.Commande;
import Entities.Event;
import Entities.User;
import IBusiness.CommandeIBusiness;
import IBusiness.EventIBusiness;
import IBusiness.UserIBusiness;
import IBusiness.VilleIBusiness;
import dtoClients.ClientDTO;

@ManagedBean(name="mbUser")
@SessionScoped
public class UserMb implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	private UserIBusiness proxyUser;
	
	@EJB
	private EventIBusiness proxyEvent;
	
	@EJB
	private CommandeIBusiness proxyCommande;

	@EJB
	private VilleIBusiness proxyVille;
	
	private String DATE_FORMAT = "dd.MM.yyyy"; // format date utilisé dans tous les dataTableColumn de type date
	
	private List<User> users = new ArrayList<>();
	private List<Event> events = new ArrayList<>();
	private List<Commande> commandes =new ArrayList<>();
	private Double totalCommande = 0d;
	private double prixTotal=0d;
	private User userEvent = new User();
	private User ajoutUser = new User();
	private boolean existeMail = false;
	private boolean existeLogin = false;
	
	public String addUser() {
		ajoutUser.setDateInscription(new Date());
		proxyUser.createUser(ajoutUser);
		ajoutUser = new User();
		ajoutUser.setClientDTO(new ClientDTO());
		return "/connexion.xhtml";
	}
	
	@PostConstruct
	public void init() {
		try{
			users = proxyUser.getAll();
			ajoutUser.setClientDTO(new ClientDTO());
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public List<Event> eventByIdUser(int id) {
		events=proxyEvent.getAllByIdUser(id);
		userEvent = proxyUser.find(id);
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext,null,"/admin/adminHistoEvent.xhtml?faces-redirect=true");
		return events;
	}

	public void checkMail() {
		if (ajoutUser.getAdresseMail() != null) {
			existeMail = proxyUser.existeDejaMail(ajoutUser.getAdresseMail());
		}

	}

	public void checkLogin() {
		if (ajoutUser.getLogin() != null) {
			existeLogin = proxyUser.existeDejaLogin(ajoutUser.getLogin());
		}
	}

	public List<Commande> commandeByIdEvent(int id){
		commandes = proxyCommande.findByIdEvent(id);
		prixTotal=0d;
		//calcul periode
		Event toto=proxyEvent.find(id);

		long diffInMillies = Math.abs(toto.getDateDebut().getTime() - toto.getDateFin().getTime());
		Long diDi = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS)+1;
		for (Commande commande : commandes) {

			prixTotal+=(commande.getArticle().getPrix()*commande.getQuantite()*diDi);
		}
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext,null,"/admin/adminHistoCommande.xhtml?faces-redirect=true");
		return commandes;
	}
	
	

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public UserIBusiness getProxyUser() {
		return proxyUser;
	}

	public void setProxyUser(UserIBusiness proxyUser) {
		this.proxyUser = proxyUser;
	}

	public User getAjoutUser() {
		return ajoutUser;
	}

	public void setAjoutUser(User ajoutUser) {
		this.ajoutUser = ajoutUser;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public EventIBusiness getProxyEvent() {
		return proxyEvent;
	}

	public void setProxyEvent(EventIBusiness proxyEvent) {
		this.proxyEvent = proxyEvent;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public Double getTotalCommande() {
		return totalCommande;
	}

	public void setTotalCommande(Double totalCommande) {
		this.totalCommande = totalCommande;
	}

	public double getPrixTotal() {
		return prixTotal;
	}

	public void setPrixTotal(double prixTotal) {
		this.prixTotal = prixTotal;
	}

	public CommandeIBusiness getProxyCommande() {
		return proxyCommande;
	}

	public void setProxyCommande(CommandeIBusiness proxyCommande) {
		this.proxyCommande = proxyCommande;
	}

	public VilleIBusiness getProxyVille() {
		return proxyVille;
	}

	public void setProxyVille(VilleIBusiness proxyVille) {
		this.proxyVille = proxyVille;
	}

	public User getUserEvent() {
		return userEvent;
	}

	public void setUserEvent(User userEvent) {
		this.userEvent = userEvent;
	}

	public String getDATE_FORMAT() {
		return DATE_FORMAT;
	}

	public void setDATE_FORMAT(String DATE_FORMAT) {
		this.DATE_FORMAT = DATE_FORMAT;
	}

	public boolean isExisteMail() {
		return existeMail;
	}

	public void setExisteMail(boolean existeMail) {
		this.existeMail = existeMail;
	}

	public boolean isExisteLogin() {
		return existeLogin;
	}

	public void setExisteLogin(boolean existeLogin) {
		this.existeLogin = existeLogin;
	}


}


