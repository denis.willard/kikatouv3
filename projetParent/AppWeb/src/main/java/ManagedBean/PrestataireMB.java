package ManagedBean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import Dto.Periode;
import Dto.Prestataire;
import Entities.Prestation;
import IBusiness.PrestationIBusiness;
import IBusiness.delegate.PrestataireIBusinessDelegate;

@ManagedBean(name = "mbPrestataire")
@SessionScoped
public class PrestataireMB implements Serializable {


    private static final long serialVersionUID = 1L;

    @EJB
    private PrestataireIBusinessDelegate proxyPrestataire;

    @EJB
    private PrestationIBusiness proxyPrestation;

    @ManagedProperty(value = "#{mblogin}")
    private LoginMb loginMb;
    private Prestataire prestataire = new Prestataire();
    private Prestation ajoutPrestation = new Prestation();


    private Date dateDebut;
    private Integer heures;
    private Date dateFin;

    private Periode periode = new Periode();

    private int idPrestation;
    private String libellePrestation;
    private List<Prestataire> AllPrestataires = new ArrayList<>();

    //@PostConstruct
    public void init() {
        if (heures != null) {
            dateFin = getDateFinFromDuree(dateDebut, heures);
        }
        if ((periode.getDateDebut() != null) && (periode.getDateFin() != null)) {
            AllPrestataires = proxyPrestataire.prestatairesParTypePrestationEtPeriode(idPrestation, periode);
        } else if (idPrestation != 0) {
            AllPrestataires = proxyPrestataire.tousPrestatairesParTypePrestation(idPrestation);
        } else {
            AllPrestataires = proxyPrestataire.tousPrestataires();
        }
    }

    public void prestatairesDispo() throws ParseException {
        periode = new Periode();
        DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
        periode.setDateDebut(shortDateFormat.format(dateDebut));
        dateFin = getDateFinFromDuree(dateDebut, heures);
        periode.setDateFin(shortDateFormat.format(dateFin));
        init();
    }

    public Date getDateFinFromDuree(Date dateDebut, int duree) {
        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(dateDebut); // sets calendar time/date
        cal.add(Calendar.HOUR_OF_DAY, duree); // adds one hour
        return cal.getTime();
    }

    public String addPrestation(int idPrestataire) {
        if (loginMb.getEvent() != null) {
            periode = new Periode();
            DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
            periode.setDateDebut(shortDateFormat.format(dateDebut));
            dateFin = getDateFinFromDuree(dateDebut, heures);
            periode.setDateFin(shortDateFormat.format(dateFin));
            System.out.println("Période début " + periode.getDateDebut());
            System.out.println("Période fin " + periode.getDateFin());
            prestataire = proxyPrestataire.reserverPrestatairesPourPeriode(idPrestataire, periode);
            init();
            ajoutPrestation.setEvent(loginMb.getEvent());
            ajoutPrestation.setIdPrestataire(idPrestataire);
            ajoutPrestation.setPrestataire(prestataire);
            ajoutPrestation.setDuree(heures);
            ajoutPrestation.setPrixHoraire(prestataire.getTauxHoraire());
            ajoutPrestation.setDateDebut(dateDebut);
            ajoutPrestation.setDateFin(dateFin);
            proxyPrestation.create(ajoutPrestation);
            ajoutPrestation = new Prestation();
            return "";
        } else if (loginMb.getUser() == null) {
            return "/connexion.xhtml";
        } else {
            return "/test/ajoutEventTest.xhtml";
        }
    }

    // Getters Setters --------------------------------------


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public PrestataireIBusinessDelegate getProxyPrestataire() {
        return proxyPrestataire;
    }

    public void setProxyPrestataire(PrestataireIBusinessDelegate proxyPrestataire) {
        this.proxyPrestataire = proxyPrestataire;
    }

    public PrestationIBusiness getProxyPrestation() {
        return proxyPrestation;
    }

    public void setProxyPrestation(PrestationIBusiness proxyPrestation) {
        this.proxyPrestation = proxyPrestation;
    }

    public LoginMb getLoginMb() {
        return loginMb;
    }

    public void setLoginMb(LoginMb loginMb) {
        this.loginMb = loginMb;
    }

    public Prestataire getPrestataire() {
        return prestataire;
    }

    public void setPrestataire(Prestataire prestataire) {
        this.prestataire = prestataire;
    }

    public Prestation getAjoutPrestation() {
        return ajoutPrestation;
    }

    public void setAjoutPrestation(Prestation ajoutPrestation) {
        this.ajoutPrestation = ajoutPrestation;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Periode getPeriode() {
        return periode;
    }

    public void setPeriode(Periode periode) {
        this.periode = periode;
    }

    public int getIdPrestation() {
        return idPrestation;
    }

    public void setIdPrestation(int idPrestation) {
        this.idPrestation = idPrestation;
    }

    public String getLibellePrestation() {
        return libellePrestation;
    }

    public void setLibellePrestation(String libellePrestation) {
        this.libellePrestation = libellePrestation;
    }

    public List<Prestataire> getAllPrestataires() {
        return AllPrestataires;
    }

    public void setAllPrestataires(List<Prestataire> allPrestataires) {
        AllPrestataires = allPrestataires;
    }

    public Integer getHeures() {
        return heures;
    }

    public void setHeures(Integer heures) {
        this.heures = heures;
    }
}
