package dtoClients;

import java.io.Serializable;

public class AuthentificationDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    public AuthentificationDTO() {
    }

    private Integer id;
    private String role;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isAuthValid() {
        return id != null && role != null;
    }
}
