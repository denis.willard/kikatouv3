package dtoClients;

import java.io.Serializable;

public class TransactionDTO implements Serializable {

    public TransactionDTO() {
    }

    public TransactionDTO(Double montant, String url) {
        this.montant = montant;
        this.url = url;
    }

    Double montant;
    String url;

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
