package Entities;

import dtoClients.ClientDTO;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

@Entity
@Table(name = "user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    public User() {
    }

    public User(List<Event> events, List<Message> messages) {
        this.events = events;
        this.messages = messages;
    }

    public User(String nom, String prenom, Date dateDeNaissance, String numTelPortable, String numTelFixe, String adresseMail, String adresse, Date dateInscription, Date dateDesinscription, String login, String password, String ville, String codePostal, ClientDTO clientDTO, List<Event> events, List<Message> messages) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateDeNaissance = dateDeNaissance;
        this.numTelPortable = numTelPortable;
        this.numTelFixe = numTelFixe;
        this.adresseMail = adresseMail;
        this.adresse = adresse;
        this.dateInscription = dateInscription;
        this.dateDesinscription = dateDesinscription;
        this.login = login;
        this.password = password;
        this.ville = ville;
        this.codePostal = codePostal;
        this.clientDTO = clientDTO;
        this.events = events;
        this.messages = messages;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Transient
    private String nom;

    @Transient
    private String prenom;

    @Transient
    private Date dateDeNaissance;

    @Transient
    private String numTelPortable;

    @Transient
    private String numTelFixe;

    @Transient
    private String adresseMail;

    @Transient
    private String adresse;

    @Transient
    private Date dateInscription;

    @Transient
    private Date dateDesinscription;

    @Transient
    private String login;

    @Transient
    private String password;

    @Transient
    private String ville;

    @Transient
    private String codePostal;

    @Transient
    private ClientDTO clientDTO;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Event> events;

    @OneToMany(mappedBy = "user")
    private List<Message> messages;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
        clientDTO.setIdUtilisateur(id);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCodePostal() {
        return clientDTO.getCodePostal();
    }

    public void setCodePostal(String codePostal) {
        this.clientDTO.setCodePostal(codePostal);
    }

    public String getVille() {
        return clientDTO.getVille();
    }

    public void setVille(String ville) {
        this.clientDTO.setVille(ville);
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public void setClientDTO(ClientDTO clientDTO) {
        this.clientDTO = clientDTO;
    }

    public String getNom() {
        return getClientDTO().getNom();
    }

    public void setNom(String nom) {
        this.clientDTO.setNom(nom);
    }

    public String getPrenom() {
        return clientDTO.getPrenom();
    }

    public void setPrenom(String prenom) {
        this.clientDTO.setPrenom(prenom);
    }

    public Date getDateDeNaissance() throws ParseException {
        if (clientDTO.getDateNaissance() != null) {
            return new SimpleDateFormat("yyyy-MM-dd").parse(clientDTO.getDateNaissance());
        }
        return null;
    }

    public void setDateDeNaissance(Date dateDeNaissance) {
        this.clientDTO.setDateNaissance(new SimpleDateFormat("yyyy-MM-dd").format(dateDeNaissance));
    }

    public String getNumTelPortable() {
        return clientDTO.getNumTelPortable();
    }

    public void setNumTelPortable(String numTelPortable) {
        this.clientDTO.setNumTelPortable(numTelPortable);
    }

    public String getNumTelFixe() {
        return clientDTO.getNumTelFixe();
    }

    public void setNumTelFixe(String numTelFixe) {
        this.clientDTO.setNumTelFixe(numTelFixe);
    }

    public String getAdresseMail() {
        return clientDTO.getEmail();
    }

    public void setAdresseMail(String adresseMail) {
        this.clientDTO.setEmail(adresseMail);
    }

    public String getAdresse() {
        return clientDTO.getAdresse();
    }

    public void setAdresse(String adresse) {
        this.clientDTO.setAdresse(adresse);
    }

    public Date getDateInscription() throws ParseException {
        if (clientDTO.getDateInscription() != null) {
            return new SimpleDateFormat("yyyy-MM-dd").parse(clientDTO.getDateInscription());
        }
        return null;
    }

    public void setDateInscription(Date dateInscription) {
        this.clientDTO.setDateInscription(new SimpleDateFormat("yyyy-MM-dd").format(dateInscription));
    }

    public Date getDateDesinscription() throws ParseException {
        if (clientDTO.getDateDesinscription() != null) {
            return new SimpleDateFormat("dd.MM.yyyy").parse(clientDTO.getDateDesinscription());
        }
        return null;
    }

    public void setDateDesinscription(Date dateDesinscription) {
        this.clientDTO.setDateDesinscription(new SimpleDateFormat("yyyy-MM-dd").format(dateDesinscription));
    }

    public String getLogin() {
        return clientDTO.getLogin();
    }

    public void setLogin(String login) {
        this.clientDTO.setLogin(login);
    }

    public String getPassword() {
        return clientDTO.getPassword();
    }

    public void setPassword(String password) {
        this.clientDTO.setPassword(password);
    }

    public void setClientDTOWebService() {
        Client client = ClientBuilder.newClient();
        ClientDTO clientDTO = client.target("http://localhost:9490/Orchestrateur/client")
                .path(id.toString())
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(ClientDTO.class);

        this.setClientDTO(clientDTO);
    }
}
