package Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Table(name = "event")
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "intitule")
    private String intitule;

    @Column(name = "dateDebut")
    @Temporal(TemporalType.DATE)
    private Date dateDebut;

    @Column(name = "dateFin")
    @Temporal(TemporalType.DATE)
    private Date dateFin;

    @Column(name = "dateValidation")
    @Temporal(TemporalType.DATE)
    private Date dateValidation;

    @Column(name = "rueLivraison")
    private String rueLivraison;

    @Column(name = "dateRetour")
    @Temporal(TemporalType.DATE)
    private Date dateRetour;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL)
    private List<Commande> commandes;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.ALL)
    private List<Prestation> prestations;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Ville ville;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User user;
    
	@ManyToOne
	@JoinColumn(referencedColumnName="id")
	private TypeEvent typeEvent;

    public Integer getId() {
        return id;
    }

    public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateDebut() {
        return dateDebut;
    }


    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Date getDateValidation() {
        return dateValidation;
    }

    public void setDateValidation(Date dateValidation) {
        this.dateValidation = dateValidation;
    }

    public String getRueLivraison() {
        return rueLivraison;
    }


    public void setRueLivraison(String rueLivraison) {
        this.rueLivraison = rueLivraison;
    }

    public Date getDateRetour() {
        return dateRetour;
    }

    public void setDateRetour(Date dateRetour) {
        this.dateRetour = dateRetour;
    }

    public List<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }

    public List<Prestation> getPrestations() {
        return prestations;
    }

    public void setPrestations(List<Prestation> prestations) {
        this.prestations = prestations;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public User getUser() {
        if (user.getClientDTO() == null) {
            user.setClientDTOWebService();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TypeEvent getTypeEvent() {
		return typeEvent;
	}

	public void setTypeEvent(TypeEvent typeEvent) {
		this.typeEvent = typeEvent;
	}

	public Event(Integer id, String intitule, Date dateDebut, Date dateFin, Date dateValidation, String rueLivraison, Date dateRetour,
                 Ville ville, User user, TypeEvent typeEvent) {
        super();
        this.id = id;
        this.intitule = intitule;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.dateValidation = dateValidation;
        this.rueLivraison = rueLivraison;
        this.dateRetour = dateRetour;
        this.ville = ville;
        this.user = user;
        this.typeEvent = typeEvent;
    }

    public Event() {
        super();
        // TODO Auto-generated constructor stub
    }
}
