package Entities;

import Dto.Prestataire;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="prestation")
public class Prestation implements Serializable {

    public Prestation() {
    }

    public Prestation(Integer idPrestataire, Event event, Double prixHoraire, int duree, Date dateDebut, Date dateFin, Prestataire prestataire) {
        this.idPrestataire = idPrestataire;
        this.event = event;
        this.prixHoraire = prixHoraire;
        this.duree = duree;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.prestataire = prestataire;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "id_prestataire")
    private Integer idPrestataire;

    @ManyToOne
    @JoinColumn(referencedColumnName="id")
    private Event event;

    @Column(name = "prix_horaire")
    private Double prixHoraire;

    @Column(name = "duree")
    private int duree;

    @Column(name ="date_debut")
    private Date dateDebut;

    @Column(name ="date_fin")
    private Date dateFin;

    @Transient
    private Prestataire prestataire;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPrestataire() {
        return idPrestataire;
    }

    public void setIdPrestataire(Integer idPrestataire) {
        this.idPrestataire = idPrestataire;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Double getPrixHoraire() {
        return prixHoraire;
    }

    public void setPrixHoraire(Double prixHoraire) {
        this.prixHoraire = prixHoraire;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public Prestataire getPrestataire() {
        return prestataire;
    }

    public void setPrestataire(Prestataire prestataire) {
        this.prestataire = prestataire;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }
}
