package dtoClients;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Set;

public class CiviliteDTO implements Serializable {
	private static final long serialVersionUID = 1L;

    public CiviliteDTO() {
    }

    public CiviliteDTO(String libelle) {
        this.libelle = libelle;
    }

    private Integer idCivilite;
    private String libelle;
    @JsonIgnore
    private Set<UtilisateurDTO> utilisateurs;

    public Integer getIdCivilite() {
        return idCivilite;
    }

    public void setIdCivilite(Integer idCivilite) {
        this.idCivilite = idCivilite;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<UtilisateurDTO> getUtilisateurs() {
        return utilisateurs;
    }

    public void setUtilisateurs(Set<UtilisateurDTO> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }
}