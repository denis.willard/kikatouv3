package dtoClients;

import java.io.Serializable;

public class AgendaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    private Integer id;
    private String nom;
    private String color;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
}
