package dtoClients;

import java.io.Serializable;
import java.util.Date;

public class PrestataireDTO  extends UtilisateurDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	//private Integer id;
	private String description;
	private String telPortable;
	private String telFixe;
	private Double tauxHoraire;
	private String photo;
	private Date dateInscription;
	private Date dateDesinscription;
	private Integer typePrestation;
	
//	public Integer getId() {
//		return id;
//	}
//	public void setId(Integer id) {
//		this.id = id;
//	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTelPortable() {
		return telPortable;
	}
	public void setTelPortable(String telPortable) {
		this.telPortable = telPortable;
	}	
	public String getTelFixe() {
		return telFixe;
	}
	public void setTelFixe(String telFixe) {
		this.telFixe = telFixe;
	}
	public Double getTauxHoraire() {
		return tauxHoraire;
	}
	public void setTauxHoraire(Double tauxHoraire) {
		this.tauxHoraire = tauxHoraire;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public Date getDateInscription() {
		return dateInscription;
	}
	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}	
	public Date getDateDesinscription() {
		return dateDesinscription;
	}

	public void setDateDesinscription(Date dateDesinscription) {
		this.dateDesinscription = dateDesinscription;
	}
	public Integer getTypePrestation() {
		return typePrestation;
	}
	public void setTypePrestation(Integer typePrestation) {
		this.typePrestation = typePrestation;
	}

	@Override
	public String toString() {
		return "PrestataireInscription [nom=" + nom + ", prenom=" + prenom + ", description="
				+ description + ", email=" + email + ", telPortable=" + telPortable + ", tauxHoraire=" + tauxHoraire
				+ ", photo=" + photo + ", dateInscription=" + dateInscription + ", dateDesinscription="
				+ dateDesinscription + ", typePrestation=" + typePrestation + "]";
	}
		
}
