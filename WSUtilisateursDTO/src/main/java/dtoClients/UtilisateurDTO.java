package dtoClients;

import java.io.Serializable;

public class UtilisateurDTO implements Serializable {

    public UtilisateurDTO() {
    }

    public UtilisateurDTO(String nom, String prenom, String email, String login, String password) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.login = login;
        this.password = password;
    }

    protected Integer idUtilisateur;
    protected String nom;
    protected String prenom;
    protected String email;
    protected String login;
    protected String password;
    protected CiviliteDTO civilite;

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public CiviliteDTO getCivilite() {
        return civilite;
    }

    public void setCivilite(CiviliteDTO civilite) {
        this.civilite = civilite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}