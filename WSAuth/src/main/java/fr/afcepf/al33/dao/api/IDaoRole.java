package fr.afcepf.al33.dao.api;

import fr.afcepf.al33.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDaoRole extends JpaRepository<Role, Integer> {

    public Role findRoleByLibelle(String libelle);

}
