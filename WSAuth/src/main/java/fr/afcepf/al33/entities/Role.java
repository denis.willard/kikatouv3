package fr.afcepf.al33.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="idRole", scope = Role.class)
public class Role implements Serializable {

    public Role() {
    }

    public Role(String libelle, Set<Authentification> authentifications) {
        this.libelle = libelle;
        this.authentifications = authentifications;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    protected Integer idRole;

    @Column(nullable = false, length = 50)
    protected String libelle;

    @OneToMany(mappedBy="role")
    @JsonIgnore
    private Set<Authentification> authentifications;

    public Integer getIdRole() {
        return idRole;
    }

    public void setIdRole(Integer idRole) {
        this.idRole = idRole;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<Authentification> getAuthentifications() {
        return authentifications;
    }

    public void setAuthentifications(Set<Authentification> authentifications) {
        this.authentifications = authentifications;
    }
}