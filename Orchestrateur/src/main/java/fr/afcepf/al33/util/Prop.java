package fr.afcepf.al33.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Prop {

    @Value("${ws.utilisateur.ajouter}")
    public String wsUserAjouter;
    @Value("${ws.utilisateur.ismail}")
    public String wsUserIsMail;
    @Value("${ws.utilisateur.islogin}")
    public String wsUserIsLogin;
    @Value("${ws.utilisateur.desinscrire}")
    public String wsUserDesinscrire;
    @Value("${ws.utilisateur.modifier}")
    public String wsUserModifier;
    @Value("${ws.utilisateur.clients}")
    public String wsUserClients;
    @Value("${ws.utilisateur.client}")
    public String wsUserClient;
    @Value("${ws.utilisateur.admin}")
    public String wsUserAdmin;

    @Value("${ws.auth.connexion}")
    public String wsAuthConnexion;
    @Value("${ws.auth.ajouter}")
    public String wsAuthAjouter;
    @Value("${ws.auth.supprimer}")
    public String wsAuthSupprimer;
    
    @Value("${ws.presta.inscription}")
    public String wsPrestaAjouter;
    @Value("${ws.agenda.ajouter}")
    public String wsAgendaAjouter;

}
