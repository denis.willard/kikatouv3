package fr.afcepf.al33.dto;

import java.io.Serializable;
import java.util.Date;

public class InscriptionPrestataireDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String login;
	private String nom;
	private String prenom;
	private String description;
	private String email;
	private String telPortable;
	private String telFixe;
	private Double tauxHoraire;
	private String photo;
	private Date dateInscription;
	private Date dateDesinscription;
	private Integer typePrestation;
	
	public Integer getId() {
		return id;
	}
		public void setId(Integer id) {
		this.id = id;
	}
		public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelPortable() {
		return telPortable;
	}
	public void setTelPortable(String telPortable) {
		this.telPortable = telPortable;
	}	
	public String getTelFixe() {
		return telFixe;
	}
	public void setTelFixe(String telFixe) {
		this.telFixe = telFixe;
	}
	public Double getTauxHoraire() {
		return tauxHoraire;
	}
	public void setTauxHoraire(Double tauxHoraire) {
		this.tauxHoraire = tauxHoraire;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public Date getDateInscription() {
		return dateInscription;
	}
	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}	
	public Date getDateDesinscription() {
		return dateDesinscription;
	}

	public void setDateDesinscription(Date dateDesinscription) {
		this.dateDesinscription = dateDesinscription;
	}
	public Integer getTypePrestation() {
		return typePrestation;
	}
	public void setTypePrestation(Integer typePrestation) {
		this.typePrestation = typePrestation;
	}
	
	@Override
	public String toString() {
		return "InscriptionPrestataireDTO [id=" + id + ", login=" + login + ", nom=" + nom + ", prenom=" + prenom
				+ ", description=" + description + ", email=" + email + ", telPortable=" + telPortable + ", telFixe="
				+ telFixe + ", tauxHoraire=" + tauxHoraire + ", photo=" + photo + ", dateInscription=" + dateInscription
				+ ", dateDesinscription=" + dateDesinscription + ", typePrestation=" + typePrestation + "]";
	}
		
}
