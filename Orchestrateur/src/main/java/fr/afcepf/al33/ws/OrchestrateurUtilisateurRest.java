package fr.afcepf.al33.ws;

import dtoClients.AgendaDTO;
import dtoClients.ClientDTO;
import dtoClients.ClientListDTO;
import dtoClients.PrestataireDTO;
import fr.afcepf.al33.dto.*;
import fr.afcepf.al33.util.Prop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value="/client" , headers="Accept=application/json")
public class OrchestrateurUtilisateurRest {

    @Autowired
    private Prop prop;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @GetMapping("/connexion/{login}/{mdp}")
    public ConnexionDTO connexionUtilisateur(@PathVariable String login,@PathVariable String mdp) {
        ConnexionDTO connexionDTO = new ConnexionDTO();
        AuthentificationRoleDTO authentificationRoleDTO = restTemplate().getForObject(prop.wsAuthConnexion + login + "/" + mdp, AuthentificationRoleDTO.class);
        if (!authentificationRoleDTO.isAuth()) {
            connexionDTO.setConnexionOK(false);
            return connexionDTO;
        } else {
            connexionDTO.setConnexionOK(true);
            ClientDTO clientDTO = new ClientDTO();
            switch (authentificationRoleDTO.getRole()) {
                case "Admin":
                    System.out.println("CONNEXION ADMIN");
                    clientDTO =  restTemplate().getForObject(prop.wsUserAdmin + authentificationRoleDTO.getId().toString(), ClientDTO.class);
                    clientDTO.setRole("Admin");
                    break;
                case "Client":
                    System.out.println("CONNEXION CLIENT");
                    clientDTO =  restTemplate().getForObject(prop.wsUserClient + authentificationRoleDTO.getId().toString(), ClientDTO.class);
                    clientDTO.setRole("Client");
                    break;
                case "Prestataire":
                    System.out.println("CONNEXION PRESTATAIRE");
                    clientDTO =  restTemplate().getForObject(prop.wsUserClient + authentificationRoleDTO.getId().toString(), ClientDTO.class);
                    clientDTO.setRole("Prestataire");
                    break;
            }
            connexionDTO.setClientDTO(clientDTO);
            return connexionDTO;
        }
    }

    @PostMapping("/inscription")
    public ClientDTO inscrireClient(@RequestBody ClientDTO clientDTO) {
        AuthentificationDTO authentificationDTO = new AuthentificationDTO();
        authentificationDTO.setIdKikatou(clientDTO.getIdUtilisateur());
        authentificationDTO.setLogin(clientDTO.getLogin());
        authentificationDTO.setPassword(clientDTO.getPassword());
        authentificationDTO.setRole("Client");
        restTemplate().postForObject(prop.wsAuthAjouter, authentificationDTO, AuthentificationDTO.class);
        ClientDTO clientDTORetour = restTemplate().postForObject(prop.wsUserAjouter, clientDTO, ClientDTO.class);
        return clientDTORetour;
    }

    @PostMapping("/inscriptionprestataire")
    public PrestataireDTO inscrirePrestataire(@RequestBody PrestataireDTO prestataireDTO) {

        InscriptionPrestataireDTO inscriptionPrestataireDTO = new InscriptionPrestataireDTO();
        inscriptionPrestataireDTO.setId(prestataireDTO.getIdUtilisateur());
        inscriptionPrestataireDTO.setNom(prestataireDTO.getNom());
        inscriptionPrestataireDTO.setPrenom(prestataireDTO.getPrenom());
        inscriptionPrestataireDTO.setDescription(prestataireDTO.getDescription());
        inscriptionPrestataireDTO.setEmail(prestataireDTO.getEmail());
        inscriptionPrestataireDTO.setTelPortable(prestataireDTO.getTelPortable());
        inscriptionPrestataireDTO.setTelFixe(prestataireDTO.getTelFixe());
        inscriptionPrestataireDTO.setTauxHoraire(prestataireDTO.getTauxHoraire());
        inscriptionPrestataireDTO.setPhoto(prestataireDTO.getPhoto());
        inscriptionPrestataireDTO.setDateInscription(prestataireDTO.getDateInscription());
        inscriptionPrestataireDTO.setDateDesinscription(prestataireDTO.getDateDesinscription());
        inscriptionPrestataireDTO.setTypePrestation(prestataireDTO.getTypePrestation());
        inscriptionPrestataireDTO.setLogin(prestataireDTO.getLogin());
        InscriptionPrestataireDTO prestataireDTORetour = restTemplate().postForObject(prop.wsPrestaAjouter, inscriptionPrestataireDTO, InscriptionPrestataireDTO.class);
    	
    	AuthentificationDTO authentificationDTO = new AuthentificationDTO();
        authentificationDTO.setIdKikatou(prestataireDTORetour.getId());
        authentificationDTO.setLogin(prestataireDTO.getLogin());
        authentificationDTO.setPassword(prestataireDTO.getPassword());
        authentificationDTO.setRole("Prestataire");
        restTemplate().postForObject(prop.wsAuthAjouter, authentificationDTO, AuthentificationDTO.class);

    	AgendaDTO agendaDTO = new AgendaDTO();
    	agendaDTO.setId(prestataireDTORetour.getId());
    	agendaDTO.setNom(prestataireDTO.getLogin());
    	agendaDTO.setColor("red");
        restTemplate().postForObject(prop.wsAgendaAjouter, agendaDTO, AgendaDTO.class);
        
        prestataireDTO.setIdUtilisateur(prestataireDTORetour.getId());
        return prestataireDTO;
    }

    @PutMapping("/modification")
    public MessageDTO modifierClient(@RequestBody ClientDTO clientDTOAModifier) {
        MessageDTO messageDTO = new MessageDTO();
        ClientDTO clientDTO = restTemplate().getForObject(prop.wsUserClient + clientDTOAModifier.getIdUtilisateur(), ClientDTO.class);
        if (!clientDTOAModifier.getEmail().equalsIgnoreCase(clientDTO.getEmail())) {
            boolean isMailExist = restTemplate().getForObject(prop.wsUserIsMail + clientDTOAModifier.getEmail(), Boolean.TYPE);
            if (isMailExist) {
                messageDTO.setMessage("Modification impossible : email déjà utilisé");
                return messageDTO;
            }
        }
        if (!clientDTOAModifier.getLogin().equalsIgnoreCase(clientDTO.getLogin())) {
            boolean isLoginExist = restTemplate().getForObject(prop.wsUserIsLogin + clientDTOAModifier.getLogin(), Boolean.TYPE);
            if (isLoginExist) {
                messageDTO.setMessage("Modification impossible : login déjà utilisé");
                return messageDTO;
            } else {
                // TODO Modifier Client dans WS Authentification
            }
        }
        Map<String, String> params = new HashMap<>();
        params.put("idUtilisateur", clientDTOAModifier.getIdUtilisateur().toString());
        restTemplate().put(prop.wsUserModifier, clientDTOAModifier, params);
        messageDTO.setMessage("Profil utilisateur modifié");
        return messageDTO;
    }

    @DeleteMapping("/desinscription/{idUtilisateur}")
    public MessageDTO desinscrireUtilisateur(@PathVariable Integer idUtilisateur) {
        MessageDTO messageDTO = new MessageDTO();
        Map<String, String> paramsUser = new HashMap<>();
        paramsUser.put("idUtilisateur", idUtilisateur.toString());
        restTemplate().delete(prop.wsUserDesinscrire + idUtilisateur, paramsUser);
        messageDTO.setMessage("Compte supprimé avec succès");
        Map<String, String> paramsAuth = new HashMap<>();
        paramsAuth.put("idKikatou", idUtilisateur.toString());
        restTemplate().delete(prop.wsAuthSupprimer + idUtilisateur, paramsAuth);
        return messageDTO;
    }

    @GetMapping("")
    public ClientListDTO getAllClient() {
        ClientListDTO clientListDTO = restTemplate().getForObject(prop.wsUserClients, ClientListDTO.class);
        return clientListDTO;
    }

    @GetMapping("/{idUtilisateur}")
    public ClientDTO getClientById(@PathVariable Integer idUtilisateur) {
        ClientDTO clientDTO = restTemplate().getForObject(prop.wsUserClient + idUtilisateur, ClientDTO.class);
        return clientDTO;
    }

}
