package fr.afcepf.al33.impl;

import fr.afcepf.al33.api.IWSMail;
import fr.afcepf.al33.dto.InfoMailDTO;

import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@WebService(targetNamespace = "http://wsmail.afcepf.fr", endpointInterface = "fr.afcepf.al33.api.IWSMail")
@Remote(IWSMail.class)
@Stateless
public class WSMail implements IWSMail {

    @Override
    public boolean envoyerMail(InfoMailDTO infoMailDTO) {

        Properties propMail = new Properties();
        String propFileName = "mail.properties";

        InputStream is = getClass().getClassLoader().getResourceAsStream(propFileName);

        try {
            propMail.load(is);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("WSMail n'a pas pu charger le fichier mail.properties!");
        }

        final String username = propMail.getProperty("mail.kikatou");
        final String password = propMail.getProperty("password.kikatou");

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            // Create a default MimeMessage object.
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(username));

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(infoMailDTO.getAdressePrestataire()));

            // Set Subject: header field
            message.setSubject("Réservation KikaTou");

            // This mail has 2 part, the BODY and the embedded image
            MimeMultipart multipart = new MimeMultipart("related");

            // first part (the html)
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            String content = "<head>" +
                    "<style type='text/css'>" +
                    "  .red { color: #f00; }" +
                    "</style>" +
                    "</head>" +
                    "<img src='cid:image' style='height: 120px; display: inline-block;'><h1 style='color: red; display: inline-block;'>" + message.getSubject() + "</h1>" +
                    "<h2><strong>" + infoMailDTO.getPrenomPrestataire() + " " + infoMailDTO.getNomPrestataire() + ",<br/>vous êtes convié, en tant que prestataire, à participer à l'événement" +
                    " du " + infoMailDTO.getDateDebutReservation() + " organisé par " + infoMailDTO.getPrenomClient() + " " + infoMailDTO.getNomClient() + " pour une durée de " + infoMailDTO.getDuree() + "H</strong></h2>" +
                    "<br/><h3>Kikatou vous souhaite une bonne journée<h3/>";
            messageBodyPart.setContent(content, "text/html; charset=utf-8");
            // add it
            multipart.addBodyPart(messageBodyPart);

            // second part (the image)
            messageBodyPart = new MimeBodyPart();
            messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(this.getClass().getResourceAsStream("/logo.png"), "image/png")));

            messageBodyPart.setHeader("Content-ID", "<image>");

            // add image to the multipart
            multipart.addBodyPart(messageBodyPart);

            // put everything together
            message.setContent(multipart);
            // Send message
            Transport.send(message);

            System.out.println("Sent message successfully....");
            return true;

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
