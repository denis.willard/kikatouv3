INSERT INTO `bdd_wspaiement`.`client` (`id_client`, `nom_titulaire`) VALUES ('1', 'Damien MONCELET');
INSERT INTO `bdd_wspaiement`.`client` (`id_client`, `nom_titulaire`) VALUES ('2', 'Lieve VERHEYDEN');
INSERT INTO `bdd_wspaiement`.`client` (`id_client`, `nom_titulaire`) VALUES ('3', 'Julien DI BERARDINO');
INSERT INTO `bdd_wspaiement`.`client` (`id_client`, `nom_titulaire`) VALUES ('4', 'Alexandra PIMENTA DOS SANTOS');
INSERT INTO `bdd_wspaiement`.`client` (`id_client`, `nom_titulaire`) VALUES ('5', 'Yatta');
INSERT INTO `bdd_wspaiement`.`client` (`id_client`, `nom_titulaire`) VALUES ('6', 'Packissimo');


INSERT INTO `bdd_wspaiement`.`compte_bancaire` (`id_compte`, `iban`, `solde`) VALUES ('1', '00002222', '200');
INSERT INTO `bdd_wspaiement`.`compte_bancaire` (`id_compte`, `iban`, `solde`) VALUES ('2', 'FR1430001019010020Z67067031', '3000');
INSERT INTO `bdd_wspaiement`.`compte_bancaire` (`id_compte`, `iban`, `solde`) VALUES ('3', 'FR1430001019010000Z67067036', '2467');
INSERT INTO `bdd_wspaiement`.`compte_bancaire` (`id_compte`, `iban`, `solde`) VALUES ('4', 'FR1430001019010000Z67067033', '8');
INSERT INTO `bdd_wspaiement`.`compte_bancaire` (`id_compte`, `iban`, `solde`) VALUES ('5', '42', '0');
INSERT INTO `bdd_wspaiement`.`compte_bancaire` (`id_compte`, `iban`, `solde`) VALUES ('6', '21', '0');


INSERT INTO `bdd_wspaiement`.`infos_bancaire` (`id_infos_bancaire`, `annee_expiration`, `cryptogramme`, `est_active`, `mois_expiration`, `numero_carte`, `id_client`, `id_compte`) VALUES ('1', '2019', '042', 1, '11', '0001', '1', '1');
INSERT INTO `bdd_wspaiement`.`infos_bancaire` (`id_infos_bancaire`, `annee_expiration`, `cryptogramme`, `est_active`, `mois_expiration`, `numero_carte`, `id_client`, `id_compte`) VALUES (2, 2021, '666', 1, 12, '1111222233334444', 2, 2);
INSERT INTO `bdd_wspaiement`.`infos_bancaire` (`id_infos_bancaire`, `annee_expiration`, `cryptogramme`, `est_active`, `mois_expiration`, `numero_carte`, `id_client`, `id_compte`) VALUES (3, 2020, '777', 1, 10, '5555666677778888', 3, 3);
INSERT INTO `bdd_wspaiement`.`infos_bancaire` (`id_infos_bancaire`, `annee_expiration`, `cryptogramme`, `est_active`, `mois_expiration`, `numero_carte`, `id_client`, `id_compte`) VALUES (4, 2079, '888', 1, 11, '9999000011112222', 4, 4);

