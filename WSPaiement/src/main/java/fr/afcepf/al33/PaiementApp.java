package fr.afcepf.al33;

import javax.faces.webapp.FacesServlet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class PaiementApp {

	public static void main(String[] args) {
		SpringApplication.run(PaiementApp.class, args);
	}
	   @Bean
	    public ServletRegistrationBean servletRegistrationBean() {
	        FacesServlet servlet = new FacesServlet();
	        ServletRegistrationBean servletRegistrationBean = 
	          new ServletRegistrationBean(servlet, "*.xhtml");
	        return servletRegistrationBean;
	    }
}
