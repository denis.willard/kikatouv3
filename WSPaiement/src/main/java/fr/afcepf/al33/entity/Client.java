package fr.afcepf.al33.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Client")
public class Client implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idClient;
	
	@Column
	private String nomTitulaire;
	
	@OneToMany(mappedBy = "client")
	private List<InfosBancaire> listeCartes;
	
	public Client() {
		super();
	}

	public Client(Integer idClient, String nomTitulaire) {
		super();
		this.idClient = idClient;
		this.nomTitulaire = nomTitulaire;
	}


	public Integer getIdClient() {
		return idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	public String getNomTitulaire() {
		return nomTitulaire;
	}

	public void setNomTitulaire(String nomTitulaire) {
		this.nomTitulaire = nomTitulaire;
	}

	public List<InfosBancaire> getListeCartes() {
		return listeCartes;
	}

	public void setListeCartes(List<InfosBancaire> listeCartes) {
		this.listeCartes = listeCartes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
