package fr.afcepf.al33.ws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import fr.afcepf.al33.IDao.IDaoTransaction;
import fr.afcepf.al33.dto.TransactionDTO;
import fr.afcepf.al33.entity.*;
import fr.afcepf.al33.service.IService;
import fr.afcepf.al33.util.MD5Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import ch.qos.logback.core.net.SyslogOutputStream;
import fr.afcepf.al33.IDao.IDaoClient;
import fr.afcepf.al33.IDao.IDaoCompteBancaire;
import fr.afcepf.al33.IDao.IDaoInfosBancaires;
import fr.afcepf.al33.dto.AutorisationDTO;
import fr.afcepf.al33.dto.ClientDTO;
import fr.afcepf.al33.dto.HashDTO;
import fr.afcepf.al33.dto.InfosBancaireDTO;
import fr.afcepf.al33.dto.InfosTransactionDTO;
import fr.afcepf.al33.exception.CreditException;
import fr.afcepf.al33.exception.DebitException;


@CrossOrigin(origins = {"http://localhost:5001"})
@RestController
@RequestMapping(value="/rest", headers="Accept=application/json")
public class PaiementRestCtrl {

	@Autowired
	private IDaoClient daoClient;

	@Autowired
	private IDaoInfosBancaires daoInfosBancaire;
	
	@Autowired
	private IDaoCompteBancaire daoCompteBancaire;

	@Autowired
	private IDaoTransaction daoTransaction;

	@Autowired
	private IService service;
	
	@Autowired
	private IService servicePaiement;
	
	private InfosBancaireDTO infoPaiement = new InfosBancaireDTO();
	private Transaction transaction;
	private String messageError = "";
	
	//******************************************************//

	@PostMapping("/paiement")
	public String envoieMontant(@RequestBody TransactionDTO transactionDTO) {
		Transaction transaction = new Transaction();
		transaction.setMontant(transactionDTO.getMontant());
		transaction.setUrl(transactionDTO.getUrl());
		Transaction transactionNew = service.creerTransaction(transaction);
		return transactionNew.getHash();
	}


	@GetMapping("/validation/{hash}")
	public String validationPaiement(@PathVariable String hash) {
		boolean paymentValidate = daoTransaction.findByHash(hash).isPaye();
		return paymentValidate ? "true" : "false";
	}
	
	
	
	//********************  ROQUE   **********************************//
	
	@GetMapping("/form/{hash}")
	public HashDTO getHash(@PathVariable String hash) {
		HashDTO hashDTO = new HashDTO();
		double montant = 0;
		Transaction transaction = daoTransaction.findByHash(hash);
	    montant = transaction.getMontant();
	    hashDTO.setMontant(montant);
	    return hashDTO;
	}
	
	@GetMapping("/payer/{numero}")
	public AutorisationDTO payer(@PathVariable String numero) {
		System.out.println("Aqui estoy en payer *******");
		System.out.println(numero);
		String num = "1111222233334444";
		/*infoPaiement.setMontant(transaction.getMontant());
        AutorisationDTO autorisation = servicePaiement.validationPaiement(infoPaiement);
        if (autorisation.isEstAccepte()) {
            messageError = "";
            servicePaiement.validerTransaction(transaction);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            try {
                externalContext.redirect(transaction.getUrl() + "?banque=true");
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Redirection après paiement impossible!!!");
            }
        }
        messageError = autorisation.getMessage();*/
        //return null;

			
		AutorisationDTO autorisation = new AutorisationDTO();
		if(numero.equals(num) ) {
			autorisation.setEstAccepte(true);
			System.out.println("Es verdadero");
		} else {
			autorisation.setEstAccepte(false);
			System.out.println("Es falso");
		}
		
		return autorisation;
		
	}
	
	
	//***  ROQUE - Test   ***//
	@GetMapping("/clients")
	public List<ClientDTO> index(){
		List<ClientDTO> result = new ArrayList<ClientDTO>();
		List<Client> liste = daoClient.findAll();
		for (Client client : liste) {
			ClientDTO clientDTO = new ClientDTO();
			clientDTO.setIdClient(client.getIdClient());
			clientDTO.setNomTitulaire(client.getNomTitulaire());
			result.add(clientDTO);
		}
		return result;
	}
	
	
	
	
	//******************************************************//
	
	@PostMapping("/transactionBancaire")
	public AutorisationDTO transactionBancaire(@RequestBody InfosTransactionDTO infosTransactionDTO) {
		AutorisationDTO autorisationDTO = new AutorisationDTO();
		System.out.println("numero carte dans transaction : " +infosTransactionDTO.getInfosBancaireDTO().getNumeroCarte());

		//Test de l'autorisation
		
		CompteBancaire compteVendeur = daoCompteBancaire.findByIban(infosTransactionDTO.getIbanVendeur());
		
		if(compteVendeur != null) {
			InfosBancaire infosBancaireAcheteur = daoInfosBancaire.getByNumeroCarte(infosTransactionDTO.getInfosBancaireDTO().getNumeroCarte());
			CompteBancaire compteAcheteur = daoCompteBancaire.getOne(infosBancaireAcheteur.getCompte().getIdCompte());
			double montant = infosTransactionDTO.getMontant();
			return effectuerDebitEtCredit(montant, compteAcheteur, compteVendeur);
		} else {
			//si iban pas bon
			autorisationDTO.setMessage(MessageEnum.mauvaisIban.toString());
			autorisationDTO.setEstAccepte(false);
			return autorisationDTO;
		}
	}
	
	private AutorisationDTO effectuerDebitEtCredit(double montant, CompteBancaire compteAcheteur, CompteBancaire compteVendeur) {
		AutorisationDTO autorisationDTO = new AutorisationDTO();
		
		try {
			debiter(montant, compteAcheteur);
			crediter(montant, compteVendeur);
		} catch (DebitException e) {
			autorisationDTO.setMessage(MessageEnum.problemeTransaction.toString());
			autorisationDTO.setEstAccepte(false);
		} catch(CreditException e) {
			compteAcheteur.setSolde(montant + compteAcheteur.getSolde());
			daoCompteBancaire.save(compteAcheteur);
			autorisationDTO.setMessage(MessageEnum.problemeTransaction.toString());
			autorisationDTO.setEstAccepte(false);	
		}
		autorisationDTO.setMessage(MessageEnum.transactionOK.toString());
		autorisationDTO.setEstAccepte(true);
		return autorisationDTO;
	}
	
	private void crediter(double montant, CompteBancaire compteVendeur) throws CreditException {
		
		double nouveauSolde = compteVendeur.getSolde() + montant;
		compteVendeur.setSolde(nouveauSolde);
		daoCompteBancaire.save(compteVendeur);
	}
	
	private void debiter(double montant, CompteBancaire compteAcheteur) throws DebitException {
		double nouveauSolde = compteAcheteur.getSolde() - montant;
		compteAcheteur.setSolde(nouveauSolde);
		daoCompteBancaire.save(compteAcheteur);
	}
	
	private boolean verifAuthenticite(InfosBancaire infosBancaire, Client client, InfosBancaireDTO infosBancaireDTO) {
		if(infosBancaire.isEstActive() == false) {
			return false;
		}
		else {
			if((infosBancaire.getAnneeExpiration() != infosBancaireDTO.getAnneeExpiration())
					|| (infosBancaire.getMoisExpiration() != infosBancaireDTO.getMoisExpiration())
					|| ! (infosBancaire.getCryptogramme().toUpperCase().equals(infosBancaireDTO.getCryptogramme().toUpperCase()))){
				return false;
			}
			return true;
		}
	}
	
	private boolean verifValidite(InfosBancaire infosBancaire) {
		int annee = Calendar.getInstance().get(Calendar.YEAR);
		int mois = Calendar.getInstance().get(Calendar.MONTH)+1;
		System.out.println("mois : " + mois);
		System.out.println("annee : " + annee);
		if(infosBancaire.getAnneeExpiration() > annee || (infosBancaire.getAnneeExpiration() == annee && infosBancaire.getMoisExpiration() >= mois)) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean verifSolvabilite(InfosBancaire infosBancaire, double montant) {
		CompteBancaire compteAssocie = daoCompteBancaire.getOne(infosBancaire.getCompte().getIdCompte());
		if(montant <= compteAssocie.getSolde()) {
			return true;
		} else {
			return false;
		}
	}
}
