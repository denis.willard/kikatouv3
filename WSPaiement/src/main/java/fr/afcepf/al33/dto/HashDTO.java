package fr.afcepf.al33.dto;

public class HashDTO {
	
	private Double montant;

	public HashDTO() {
		super();
		
	}

	public HashDTO(Double montant) {
		super();
		this.montant = montant;
	}

	public Double getMontant() {
		return montant;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}

}
