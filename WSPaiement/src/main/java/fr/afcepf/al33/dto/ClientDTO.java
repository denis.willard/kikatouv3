package fr.afcepf.al33.dto;

public class ClientDTO {
	
	private Integer idClient;
	private String nomTitulaire;
	
	public ClientDTO() {
		super();
	}

	public ClientDTO(Integer idClient, String nomTitulaire) {
		super();
		this.idClient = idClient;
		this.nomTitulaire = nomTitulaire;
	}

	public Integer getIdClient() {
		return idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	public String getNomTitulaire() {
		return nomTitulaire;
	}

	public void setNomTitulaire(String nomTitulaire) {
		this.nomTitulaire = nomTitulaire;
	}
	

}
