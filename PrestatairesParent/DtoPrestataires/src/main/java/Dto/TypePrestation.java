package Dto;

import java.io.Serializable;

public class TypePrestation implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String libelle;
	private String description;
	private String illustration;
	
	// Getter Setter et Constructeur --------------------------------------------------------------------------

	public TypePrestation() {
		super();
	}

	public TypePrestation(int id, String libelle, String description, String illustration) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.description = description;
		this.illustration = illustration;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIllustration() {
		return illustration;
	}
	public void setIllustration(String illustration) {
		this.illustration = illustration;
	}

	
	
}
