package Dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;


public class Periode implements Serializable
{

	private static final long serialVersionUID = 1L;
	
//	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "fr_FR")
//	@JsonDeserialize(using = DateHandler.class)
//	private Date dateDebut;
//	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "fr_FR")
//	private Date dateFin;
	private String dateDebut;
	private String dateFin;

	// Geters & Setters -----------------------------------
	
	public String getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(String dateDebut) {
		this.dateDebut = dateDebut;
	}
	public String getDateFin() {
		return dateFin;
	}
	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}
	public Periode(String dateDebut, String dateFin) {
		super();
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}
	public Periode() {
		super();
	}

}
