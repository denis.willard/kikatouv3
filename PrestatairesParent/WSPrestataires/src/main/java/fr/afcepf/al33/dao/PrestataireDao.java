package fr.afcepf.al33.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al33.entity.Prestataire;
import fr.afcepf.al33.entity.TypePrestation;

// géré par SpringBoot car dérive de CrudRepository
public interface PrestataireDao extends CrudRepository<Prestataire, Integer> 
{
	// méthodes
	List<Prestataire> findByPrestation(TypePrestation id);
	Prestataire findByLogin(String login);
	
//	List<Prestataire> findByPrestationByPeriode(TypePrestation prestation, Periode periode);
//	Prestataire updateByIdPeriode(Prestataire prestataire, Periode periode);
}
