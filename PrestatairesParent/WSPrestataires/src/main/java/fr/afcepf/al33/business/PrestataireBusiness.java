package fr.afcepf.al33.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Dto.Periode;
import fr.afcepf.al33.dao.PrestataireDao;
import fr.afcepf.al33.dao.TypePrestationDao;
import fr.afcepf.al33.dto.ListPrestataireReferences;
import fr.afcepf.al33.dto.PrestataireInscription;
import fr.afcepf.al33.dto.PrestataireReference;
import fr.afcepf.al33.entity.Prestataire;
import fr.afcepf.al33.entity.TypePrestation;

@Service
public class PrestataireBusiness implements PrestataireIBusiness {
	@Autowired
	private PrestataireDao dao;
	
	@Autowired
	private TypePrestationDao daoTypePrestation;
	
	@Autowired
	private AgendaIBusinessDelegate proxyAgenda;
	
	@Override
	public List<Prestataire> getPrestatairesByTypePrestation(Integer idPrestation) {
		List<Prestataire> result = null;
		if (idPrestation == null)
			result = (List<Prestataire>) dao.findAll();
		else {
			TypePrestation prestation = daoTypePrestation.findById(idPrestation.intValue()).orElse(null);
			if (prestation != null) 
				result = (List<Prestataire>) dao.findByPrestation(prestation);
		}
		return result;
	}
	
	@Override
	public Prestataire getPrestataire(Integer idPrestataire) {
		return (Prestataire) dao.findById(idPrestataire).orElse(null);
	}
	
	@Override
	public List<Prestataire> getPrestatairesByTypesPrestationsAndDisponibilityPeriode(Periode periode, Integer idPrestation) 
	{
		System.out.println("Dispo Date : " + idPrestation + " : " + periode.getDateDebut() + " - " + periode.getDateFin());
		List<Prestataire> result = new ArrayList<Prestataire>();
		TypePrestation prestation = daoTypePrestation.findById(idPrestation.intValue()).orElse(null);
		List<Prestataire> prestByPrestation = (List<Prestataire>) dao.findByPrestation(prestation);
		ListPrestataireReferences prestRefs = proxyAgenda.tousPrestatairesDispoParPeriode(periode);
		for (Prestataire prestataire : prestByPrestation) {
			for (PrestataireReference prestataireRef : prestRefs.getPrestatairesReferences()) {
				if (prestataire.getId() == prestataireRef.getId()) 
				{
					result.add(prestataire);
					break;
				}		
			}
		}
		return result;
	}

	@Override
	public Prestataire reserverPrestatairePourPeriode(Periode periode, Integer idPrestataire, String societe) {
		System.out.println("Réserver + Dispo " + idPrestataire + " " + periode.getDateDebut() + " " + periode.getDateFin() + " " + societe);
		Prestataire prestataire = dao.findById(idPrestataire.intValue()).orElse(null);
		proxyAgenda.reserverPrestatairePeriode( societe, idPrestataire.intValue(), periode);
		return prestataire;
	}

	@Override
	public Prestataire annulerPrestatairePourPeriode(Periode periode, Integer idPrestataire, String societe) {
		System.out.println("Annuler + Dispo " + idPrestataire + " " + periode.getDateDebut() + " " + periode.getDateFin() + " " + societe);
		Prestataire prestataire = dao.findById(idPrestataire.intValue()).orElse(null);
		proxyAgenda.annulerPrestatairePeriode( societe, idPrestataire.intValue(), periode);
		return prestataire;

	}


	@Override
	public Prestataire inscrirePrestataire(PrestataireInscription prestataireInscription) {
		System.out.println(prestataireInscription);
		TypePrestation prestation = daoTypePrestation.findById(prestataireInscription.getTypePrestation()).orElse(null);
		Prestataire prestataire = null;
		
		prestataire = dao.findById(7).orElse(null);
		System.out.println(prestataire);
				
		if (prestataireInscription.getId() == null || prestataireInscription.getId() == -1) {
			prestataire = new Prestataire();
		}
		else
		{
			prestataire = dao.findById(prestataireInscription.getId().intValue()).orElse(null);
		}
		prestataire.setLogin(prestataireInscription.getLogin());
		prestataire.setNom(prestataireInscription.getNom());
		prestataire.setPrenom(prestataireInscription.getPrenom());
		prestataire.setDescription(prestataireInscription.getDescription());
		prestataire.setEmail(prestataireInscription.getEmail());
		prestataire.setPhoto(prestataireInscription.getPhoto());
		prestataire.setTelPortable(prestataireInscription.getTelPortable());
		prestataire.setTelFixe(prestataireInscription.getTelFixe());
		//prestataire.setTelFixe(prestataireInscription.getTelFixe());
		prestataire.setTauxHoraire(prestataireInscription.getTauxHoraire());
		
		if (prestataireInscription.getDateInscription() != null) {
			prestataire.setDateInscription(prestataireInscription.getDateInscription());
			prestataire.setDateDemandeInscription(prestataireInscription.getDateInscription());
			prestataire.setDateDebutDisponibilite(prestataireInscription.getDateInscription());
		}

		prestataire.setPrestation(prestation);

		dao.save(prestataire);
		return prestataire;
	}


	public Prestataire desinscrirePrestataire(PrestataireInscription prestataireInscription) {
		TypePrestation prestation = daoTypePrestation.findById(prestataireInscription.getTypePrestation()).orElse(null);
		Prestataire prestataire = null;
		if (prestataireInscription.getId() == null) {
			prestataire = new Prestataire();
		}
		else
		{
			prestataire = dao.findById(prestataireInscription.getId().intValue()).orElse(null);
		}
		prestataire.setNom(prestataireInscription.getNom());
		prestataire.setPrenom(prestataireInscription.getPrenom());
		prestataire.setDescription(prestataireInscription.getDescription());
		prestataire.setEmail(prestataireInscription.getEmail());
		prestataire.setPhoto(prestataireInscription.getPhoto());
		prestataire.setTelPortable(prestataireInscription.getTelPortable());
		prestataire.setTelFixe(prestataireInscription.getTelFixe());
		//prestataire.setTelFixe(prestataireInscription.getTelFixe());
		prestataire.setTauxHoraire(prestataireInscription.getTauxHoraire());
		
		if (prestataireInscription.getDateDesinscription() != null) {
			prestataire.setDateDesinscription(prestataireInscription.getDateDesinscription());
			prestataire.setDateDemandeDesinscription(prestataireInscription.getDateDesinscription());
			prestataire.setDateDebutIndisponibilite(prestataireInscription.getDateDesinscription());
		}

		prestataire.setPrestation(prestation);

		dao.save(prestataire);
		return prestataire;
	}
}
