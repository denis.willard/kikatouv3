package fr.afcepf.al33.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import Dto.Periode;
import fr.afcepf.al33.business.PrestataireBusiness;
import fr.afcepf.al33.dto.PrestataireInscription;
import fr.afcepf.al33.entity.Prestataire;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/rest/public", headers="Accept=application/json")
public class PrestataireRestCtrl 
{
	@Autowired
	private PrestataireBusiness proxyPrestataire;
		
	@GetMapping(value="/prestataires")
	public List<Prestataire> getPrestatairesByTypePrestation (@RequestParam(value="idPrestation", required=false) Integer idPrestation)
	{
		return proxyPrestataire.getPrestatairesByTypePrestation(idPrestation);
	}
	
	@GetMapping(value="/prestataire")
	public Prestataire getPrestataire (@RequestParam(value="idPrestataire", required=true) Integer idPrestataire)
	{
		return proxyPrestataire.getPrestataire(idPrestataire);
	}

	
	@PostMapping(value="/prestataires/periode")
	public List<Prestataire> getPrestatairesByTypesPrestationsAndDisponibilityPeriode (@RequestBody Periode periode, 
														@RequestParam(value="idPrestation") Integer idPrestation)
	{
		System.out.println("-----> /prestataires/periode " + periode.getDateDebut());
		List<Prestataire> result = proxyPrestataire.getPrestatairesByTypesPrestationsAndDisponibilityPeriode(periode, idPrestation);
		return result;
	}	
	
	@PostMapping(value="/reserverPrestataire/periode")
	public Prestataire reserverPrestatairePourPeriode (@RequestBody Periode periode, 
														@RequestParam(value="idPrestataire") Integer idPrestataire,
														@RequestParam(value="Societe") String societe)
	{
		System.out.println("-----> reserverPrestataire/periode");
		return proxyPrestataire.reserverPrestatairePourPeriode(periode, idPrestataire, societe);
	}	
	
	@PostMapping(value="/annulerPrestataire/periode")
	public Prestataire annulerPrestatairePourPeriode (@RequestBody Periode periode, 
														@RequestParam(value="idPrestataire") Integer idPrestataire,
														@RequestParam(value="Societe") String societe)
	{
		System.out.println("-----> annulerPrestataire/periode");
		return proxyPrestataire.annulerPrestatairePourPeriode(periode, idPrestataire, societe);
	}	
	
	@PostMapping(value="/inscription")
	public Prestataire inscrirePrestataire(@RequestBody PrestataireInscription prestataireInscription) {
		System.out.println(prestataireInscription);
		return proxyPrestataire.inscrirePrestataire(prestataireInscription);
	}

	@PostMapping(value="/desinscription")
	public Prestataire annulerInscription(@RequestBody PrestataireInscription prestataireInscription) {
		return proxyPrestataire.desinscrirePrestataire(prestataireInscription);
	}
	
}
