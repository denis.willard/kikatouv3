package fr.afcepf.al33.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Prestataire 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private int id;
	private String login;
	private String nom;
	private String prenom;
	private String description;
	private String email;
	private String telPortable;
	private String telFixe;
	private Double tauxHoraire;
	private String photo;
	
	private Date dateDemandeInscription;
	private Date dateDebutDisponibilite;
	private Date dateInscription;
	private Date dateRefusInscription;
	private Date dateDemandeDesinscription;
	private Date dateDebutIndisponibilite;
	private Date dateDesinscription;
	
	@ManyToOne
	private TypePrestation prestation;
	
	// Getter Setter et Constructeur --------------------------------------------------------------------------
	
	public Prestataire() {
		super();
	}
	
	public Prestataire(int id, String nom, String prenom, String description, String email, String telPortable, String telFixe, Double tauxHoraire, String photo, TypePrestation prestation) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.description = description;
		this.email = email;
		this.telPortable = telPortable;
		this.telFixe = telFixe;
		this.tauxHoraire = tauxHoraire;
		this.photo = photo;
		this.prestation = prestation;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelPortable() {
		return telPortable;
	}
	public void setTelPortable(String telPortable) {
		this.telPortable = telPortable;
	}
	public String getTelFixe() {
		return telFixe;
	}
	public void setTelFixe(String telFixe) {
		this.telFixe = telFixe;
	}
	public Double getTauxHoraire() {
		return tauxHoraire;
	}
	public void setTauxHoraire(Double tauxHoraire) {
		this.tauxHoraire = tauxHoraire;
	}

	public TypePrestation getPrestation() {
		return prestation;
	}

	public void setPrestation(TypePrestation prestation) {
		this.prestation = prestation;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateDemandeInscription() {
		return dateDemandeInscription;
	}

	public void setDateDemandeInscription(Date dateDemandeInscription) {
		this.dateDemandeInscription = dateDemandeInscription;
	}

	public Date getDateDebutDisponibilite() {
		return dateDebutDisponibilite;
	}

	public void setDateDebutDisponibilite(Date dateDebutDisponibilite) {
		this.dateDebutDisponibilite = dateDebutDisponibilite;
	}

	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	public Date getDateRefusInscription() {
		return dateRefusInscription;
	}

	public void setDateRefusInscription(Date dateRefusInscription) {
		this.dateRefusInscription = dateRefusInscription;
	}

	public Date getDateDemandeDesinscription() {
		return dateDemandeDesinscription;
	}

	public void setDateDemandeDesinscription(Date dateDemandeDesinscription) {
		this.dateDemandeDesinscription = dateDemandeDesinscription;
	}

	public Date getDateDebutIndisponibilite() {
		return dateDebutIndisponibilite;
	}

	public void setDateDebutIndisponibilite(Date dateDebutIndisponibilite) {
		this.dateDebutIndisponibilite = dateDebutIndisponibilite;
	}

	public Date getDateDesinscription() {
		return dateDesinscription;
	}

	public void setDateDesinscription(Date dateDesinscription) {
		this.dateDesinscription = dateDesinscription;
	}
	
}
