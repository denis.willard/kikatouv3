package fr.afcepf.al33.business;

import java.util.List;

import Dto.Periode;
import fr.afcepf.al33.dto.PrestataireInscription;
import fr.afcepf.al33.entity.Prestataire;

public interface PrestataireIBusiness {
	public List<Prestataire> getPrestatairesByTypePrestation(Integer idPrestation);	
	public List<Prestataire> getPrestatairesByTypesPrestationsAndDisponibilityPeriode(Periode periode, Integer idPrestation);
	public Prestataire reserverPrestatairePourPeriode(Periode periode, Integer idPrestataire, String societe);
	Prestataire getPrestataire(Integer idPrestataire);
	Prestataire annulerPrestatairePourPeriode(Periode periode, Integer idPrestataire, String societe);
	Prestataire inscrirePrestataire(PrestataireInscription prestataireInscription);	
}
