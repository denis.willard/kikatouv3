package fr.afcepf.al33;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class WSPrestatairesAPP extends SpringBootServletInitializer {

	public static void main(String[] args) 
	{
		SpringApplication app = new SpringApplication(WSPrestatairesAPP.class);
		app.setAdditionalProfiles("web.dev");
		ConfigurableApplicationContext context = app.run(args);
	}

}
