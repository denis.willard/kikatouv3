package fr.afcepf.al33.business;

import java.util.List;

import fr.afcepf.al33.entity.TypePrestation;

public interface TypePrestationIBusiness {
	public List<TypePrestation> getTypesPrestations();
}
