package fr.afcepf.al33.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TypePrestation 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private int id;
	private String libelle;
	private String description;
	private String illustration;
	
	// Getter Setter et Constructeur --------------------------------------------------------------------------
	
	public TypePrestation() {
		super();
	}
	
	public TypePrestation(int id, String libelle, String description, String illustration) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.description = description;
		this.illustration = illustration;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIllustration() {
		return illustration;
	}
	public void setIllustration(String illustration) {
		this.illustration = illustration;
	}

	
}
