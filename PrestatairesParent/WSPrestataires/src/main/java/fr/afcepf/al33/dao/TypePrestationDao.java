package fr.afcepf.al33.dao;

import org.springframework.data.repository.CrudRepository;

import fr.afcepf.al33.entity.TypePrestation;

//géré par SpringBoot car dérive de CrudRepository
public interface TypePrestationDao extends CrudRepository<TypePrestation, Integer> 
{

}
