INSERT INTO prestataire (id, nom, color) VALUES (1, 'DJ Morad', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (2, 'Cuisinier Guillaume', 'red');
INSERT INTO prestataire (id, nom, color) VALUES (3, 'Animateur Cyril', 'green');
INSERT INTO prestataire (id, nom, color) VALUES (4, 'Roque et son orchestre', 'yellow');
INSERT INTO prestataire (id, nom, color) VALUES (5, 'Sécurité Denis', 'cyan');
INSERT INTO prestataire (id, nom, color) VALUES (6, 'Photographe Laurent', 'orange');
INSERT INTO prestataire (id, nom, color) VALUES (7, 'Photographe Sylvain', 'pink');
INSERT INTO prestataire (id, nom, color) VALUES (8, 'Animateur Florian', 'magenta');

INSERT INTO prestataire (id, nom, color) VALUES (9, 'olivier', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (10, 'ben', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (11, 'phillipe', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (12, 'julien', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (13, 'marc', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (14, 'thomas', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (15, 'bob', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (16, 'bruce', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (17, 'ted', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (18, 'johnny', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (19, 'paul', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (20, 'ed', 'blue');
INSERT INTO prestataire (id, nom, color) VALUES (21, 'danny', 'blue');


INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (1, 'cyan', '2019-06-16 18:00', '2019-06-16 22:00', 5, -1, 'KIKATOU', 'denis');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (2, 'cyan', '2019-06-17 18:00', '2019-06-17 22:00', 5, -1, 'KIKATOU', 'denis');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (3, 'cyan', '2019-06-17 12:00', '2019-06-17 15:00', 5, -1, 'KIKATOU', 'denis');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (4, 'cyan', '2019-06-18 18:00', '2019-06-18 22:00', 5, -1, 'KIKATOU', 'denis');

INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (5, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 9, -1, 'olivier', 'Indisponibilité olivier');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (6, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 10, -1, 'ben', 'Indisponibilité ben');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (7, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 11, -1, 'phillipe', 'Indisponibilité phillipe');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (8, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 12, -1, 'julien', 'Indisponibilité julien');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (9, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 13, -1, 'marc', 'Indisponibilité marc');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (10, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 14, -1, 'thomas', 'Indisponibilité thomas');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (11, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 15, -1, 'bob', 'Indisponibilité bob');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (12, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 16, -1, 'bruce', 'Indisponibilité bruce');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (13, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 17, -1, 'ted', 'Indisponibilité ted');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (14, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 18, -1, 'johnny', 'Indisponibilité johnny');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (15, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 19, -1, 'paul', 'Indisponibilité paul');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (16, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 20, -1, 'ed', 'Indisponibilité ed');
INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (17, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 21, -1, 'danny', 'Indisponibilité danny');

INSERT INTO calendar_event(id, color, start, end, prestataire_id, event_id, locked_by, title) VALUES (18, 'blue', '2019-06-24 00:00', '2019-06-30 23:00', 5, -1, 'denis2', 'Indisponibilité denis2');
