package fr.afcepf.al33.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CalendarEvent {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id", nullable=false)
	private int id;
	private int eventId;
	private int prestataireId;
    private String title;
    private Date start;
    private Date end;
    private String lockedBy; 
    private String color;

	public CalendarEvent() {
		super();
	}

	public CalendarEvent(int id, int eventId, int prestataireId, String title, Date start, Date end, String lockedBy, String color) {
		super();
		this.id = id;
		this.eventId = eventId;
		this.prestataireId = prestataireId;
		this.title = title;
		this.start = start;
		this.end = end;
		this.lockedBy = lockedBy;
		this.color = "red"; //color;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getPrestataireId() {
		return prestataireId;
	}

	public void setPrestataireId(int prestataireId) {
		this.prestataireId = prestataireId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public String getLockedBy() {
		return lockedBy;
	}

	public void setLockedBy(String lockedBy) {
		this.lockedBy = lockedBy;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	@Override
	public String toString() {
		return "[" + this.getId() + "] Start=" + this.getStart() + ", End=" + this.getEnd();
		
	}
	
	public boolean isInsidePeriode(Date deb, Date fin) {
		
		// si la date début de c est dans la periode, alors le prestaire n'est pas disponible
		if ( this.getStart().compareTo(fin) >= 0 ) { return false; } // début de réservation au-dela de la fin de période
		if ( this.getEnd().compareTo(deb) <= 0 ) { return false; } // fin de réservation en-deça du début de période
		
		return true;		
	}
}
