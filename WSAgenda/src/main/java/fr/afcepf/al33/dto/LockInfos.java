package fr.afcepf.al33.dto;

import java.io.Serializable;
import java.util.Date;

public class LockInfos implements Serializable {
	private static final long serialVersionUID = 1L; 
	
    private Date start;
    private Date end;
    private int idPrestataire;
    private String lockedBy;
    
    
	public LockInfos() {
		super();
	}

	public LockInfos(Date start, Date end, int idPrestataire, String lockedBy) {
		super();
		this.start = start;
		this.end = end;
		this.idPrestataire = idPrestataire;
		this.lockedBy = lockedBy;
	}
	
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	public int getIdPrestataire() {
		return idPrestataire;
	}
	public void setIdPrestataire(int idPrestataire) {
		this.idPrestataire = idPrestataire;
	}
	public String getLockedBy() {
		return lockedBy;
	}
	public void setLockedBy(String lockedBy) {
		this.lockedBy = lockedBy;
	}

    
}
