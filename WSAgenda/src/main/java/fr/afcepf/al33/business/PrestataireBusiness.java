package fr.afcepf.al33.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afcepf.al33.dao.PrestataireDao;
import fr.afcepf.al33.entity.Prestataire;

@Service
public class PrestataireBusiness implements PrestataireIBusiness {
	
	@Autowired
	private PrestataireDao dao;

	@Override
	public Prestataire createOrUpdatePrestataire(Prestataire prestataire) {
		dao.save(prestataire);
		return prestataire; // contenant la clé auto-incrémentée, quand gérée !
	}

}
