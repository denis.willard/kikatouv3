package fr.afcepf.al33.business;

import java.util.List;

import org.springframework.http.ResponseEntity;

import fr.afcepf.al33.dto.ListPrestataireReferences;
import fr.afcepf.al33.dto.LockInfos;
import fr.afcepf.al33.dto.Periode;
import fr.afcepf.al33.dto.PrestataireReference;
import fr.afcepf.al33.entity.CalendarEvent;

public interface CalendarEventIBusiness {
	List<CalendarEvent> tousCalendarEvents(Integer id);
	List<CalendarEvent> tousCalendarEventsByDate();
	CalendarEvent createOrUpdateCalendarEvent(CalendarEvent calendarEvent);
	ResponseEntity<?> deleteCalendarEvent(CalendarEvent calendarEvent);
	// liste des prestataires qui ont une disponibilité 
	ListPrestataireReferences getPrestatairesDisponibles(Periode periode);
	boolean estLibre(LockInfos lockInfos);
	boolean lockCalendarEvent(LockInfos lockInfos);
	boolean unlockCalendarEvent(LockInfos lockInfos);
	List<CalendarEvent> getCalendarEventLockedByMe(LockInfos lockInfos);
	List<CalendarEvent> lockedCalendarEvents(String lockedBy);
}
