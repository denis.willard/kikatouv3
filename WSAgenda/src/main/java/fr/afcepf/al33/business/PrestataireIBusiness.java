package fr.afcepf.al33.business;

import fr.afcepf.al33.entity.Prestataire;

public interface PrestataireIBusiness {
	Prestataire createOrUpdatePrestataire(Prestataire prestataire);

}
