package fr.afcepf.al33.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al33.business.PrestataireBusiness;
import fr.afcepf.al33.entity.Prestataire;

@RestController
@RequestMapping(value="/presta", headers="Accept=application/json")
public class CrudPrestataireRestCtrl {
	@Autowired
	private PrestataireBusiness proxy;

	@PostMapping(value="/post")
	public Prestataire createOrUpdateCalendarEvent(@RequestBody Prestataire prestataire) {
		return proxy.createOrUpdatePrestataire(prestataire);
	}
	
}
