package fr.afcepf.al33.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.afcepf.al33.business.CalendarEventBusiness;
import fr.afcepf.al33.dto.ListPrestataireReferences;
import fr.afcepf.al33.dto.LockInfos;
import fr.afcepf.al33.dto.Periode;
import fr.afcepf.al33.dto.PrestataireReference;
import fr.afcepf.al33.entity.CalendarEvent;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value="/dates", headers="Accept=application/json")
public class CalendarEventRestCtrl {
	
	@Autowired
	private CalendarEventBusiness proxy;

	@GetMapping(value = "/all")
	public List<CalendarEvent> tousCalendarEvents (@RequestParam(value="id", required=false) Integer id)
	{
		return proxy.tousCalendarEvents(id);
	}

	@GetMapping(value = "/locked")
	public List<CalendarEvent> lockedCalendarEvents (@RequestParam(value="lockedBy", required=false) String lockedBy)
	{
		return proxy.lockedCalendarEvents(lockedBy);
	}

	@PostMapping(value = "/dispo")
	public ListPrestataireReferences getPrestatairesDisponibles(@RequestBody Periode periode)
	{
		return proxy.getPrestatairesDisponibles(periode);
	}

}
